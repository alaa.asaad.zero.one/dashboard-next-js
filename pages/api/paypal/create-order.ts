// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios';

import PayPalOrder from '@/types/paypal-order-type';

// import GetAccessToken from './get-token';

async function GetAccessToken() {
  const data = 'grant_type=client_credentials';
  const url = process.env.PayPal_SandBox_CreateToken;
  const auth = {
    username: process.env.PAYPAL_CLIENT_ID || 'undefined',
    password: process.env.PayPal_Client_SECRET || 'undefined',
  };
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };
  try {
    const response = await axios({
      method: 'post',
      url,
      data,
      auth,
      headers,
    });
    const { access_token: accessToken } = await response.data;
    return {
      accessToken,
    };
  } catch (error) {
    return {
      message: 'Token not generated Error',
      error,
    };
  }
}
export default async function CreateOrder() {
  const { accessToken } = await GetAccessToken();
  const url = process.env.PayPal_SandBox_CreateOrder || 'undefined';
  // console.log(`Bearer ${accessToken}`);

  const headers = {
    Authorization: `Bearer ${accessToken}`,
    'Content-Type': 'application/json',
  };
  const data = {
    intent: 'CAPTURE',
    purchase_units: [
      {
        items: [
          {
            name: 'T-Shirt',
            description: 'Green XL',
            quantity: '1',
            unit_amount: {
              currency_code: 'USD',
              value: '10.00',
            },
          },
        ],
        amount: {
          currency_code: 'USD',
          value: '10.00',
          breakdown: {
            item_total: {
              currency_code: 'USD',
              value: '10.00',
            },
          },
        },
      },
    ],
    application_context: {
      return_url: process.env.PayPal_Return_URL, // this url when buyer approve the order
      cancel_url: process.env.PayPal_Cancel_URL, // this url when buyer cancel the order
    },
  };

  try {
    const response = await axios({
      method: 'post',
      url,
      data,
      headers,
    });
    const res: PayPalOrder = await response.data;

    return res;
  } catch (error) {
    return {
      message: 'Order Create Error',
      error,
    };
  }
}
